#pragma once
#include<iostream>
#include<math.h>
using namespace std;

class Pose {
protected:
	float x;
	float y;
	float th;
public:
	Pose() {};
	~Pose() {};
	float getX()const;
	void setX(float);
	float getY()const;
	void setY(float);
	void setTh(float);
	float getTh()const;
	bool operator==(const Pose&);
	Pose operator+(const Pose&);
	Pose operator-(const Pose&);
	Pose& operator+=(const Pose&);
	Pose& operator-=(const Pose&);
	bool operator<(const Pose&);
	void getPose(float&, float&, float&);
	void setPose(float, float, float);
	float findDistanceTo(Pose);
	float findAngleTo(Pose);

};
