#include"Pose.h"
using namespace std;

float Pose::getX()const { return x; }
float Pose::getY()const { return y; }
float Pose::getTh()const { return th; }
void Pose::setX(float x_) { x = x_; }
void Pose::setY(float y_) { y = y_; }
void Pose::setTh(float th_) { th = th_; }
Pose Pose:: operator+(const Pose& other) {
	Pose tmp;
	tmp.x = this->x + other.x;
	tmp.y = this->y + other.y;
	tmp.th = this->th + other.th;
	return tmp;
}
bool Pose::operator==(const Pose& other) {
	if (this->x == other.x&&this->y == other.y&&this->th == other.th) {
		return true;
	}
	else
		return false;
}
Pose Pose::operator-(const Pose& other) {
	Pose tmp;
	tmp.x = this->x - other.x;
	tmp.y = this->y - other.y;
	tmp.th = this->th - other.th;
	return tmp;
}
Pose& Pose::operator+=(const Pose& other) {
	this->x = this->x + other.x;
	this->y = this->y + other.y;
	this->th = this->th + other.th;
	return *this;
}
Pose& Pose::operator-=(const Pose& other) {
	this->x = this->x - other.x;
	this->y = this->y - other.y;
	this->th = this->th - other.th;
	return *this;
}
bool Pose::operator<(const Pose& other) {
	Pose v;
	Pose p;
	p = other;
	v.setX(0);
	v.setY(0);
	v.setTh(0);
	float t = this->findDistanceTo(v);
	float u;
	u = p.findDistanceTo(v);
	if (u < t) {
		return true;
	}
	else {
		return false;
	}

}
void Pose::getPose(float& x_, float& y_, float& th_) {
	x_ = x;
	y_ = y;
	th_ = th;
}
void Pose::setPose(float x_, float y_, float th_) {
	x = x_;
	y = y_;
	th = th_;
}
float Pose::findDistanceTo(Pose pos) {
	float distance;
	distance = (this->x - pos.x)*(this->x - pos.x);
	distance += ((this->y - pos.y)*(this->y - pos.y));
	distance = sqrt(distance);
	return distance;
}
float Pose::findAngleTo(Pose pos) {
	float tmp, tmp1, tmp2;
	tmp = (this->x*pos.x) + (this->y*pos.y);
	tmp1 = (this->x*this->x) + (this->y*this->y);
	tmp1 = sqrt(tmp1);
	tmp2 = (pos.x*pos.x) + (pos.y*pos.y);
	tmp2 = sqrt(tmp2);
	tmp = tmp / (tmp1*tmp2);
	tmp = acos(tmp) *(float)180 /  3.141592653589793238463;
	return tmp;
}
