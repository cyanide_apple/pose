
#include "Pose.h"

using namespace std;

int main() {

	Pose p1;
	Pose p2;
	p1.setX(3);
	p1.setY(4);
	p1.setTh(60);
	p2.setX(6);
	p2.setY(1);
	p2.setTh(30);
	Pose p3;
	p3 = p1 + p2;
	Pose p4;
	p4.setX(3);
	p4.setY(4);
	p4.setTh(60);
	if (p1 == p4) {
		cout << "p1=p4" << endl;
	}
	else {
		cout << "p1!=p4" << endl;
	}
	p4 = p3 - p2;
	p4 += p2;
	p3 -= p1;
	if (p2 < p4) {
		cout << "p2 < p4" << endl;
	}
	else {
		cout << "p2 != p4" << endl;
	}
	cout << "p1 and p4 distance: " << p1.findDistanceTo(p4) << endl;
	cout << "p1 and p2 angle: " << p1.findAngleTo(p2) << endl;


	system("pause");
	return 0;
}